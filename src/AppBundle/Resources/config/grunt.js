var config = module.exports = {};

config.less = {};

config.less.app = {
    files: {
        "web/static/css/app.css": "src/AppBundle/Resources/public/css/styles.less"
    }
};

config.concat = {};

/*config.concat.app_css = {
 src: ['bower_components/animate-css/animate.css','evenlead/app/assets/css/*.css'],
 dest: 'static/build/css/app.css'
 };*/

config.concat.app_js = {
    src: ['src/AppBundle/Resources/public/js/*.js'],
    dest: 'web/static/js/app.js'
};

config.cssmin = {};

config.cssmin.app = {
    src: ['web/static/css/app.css'],
    dest: 'web/static/css/app.css'
};

config.uglify = {};

config.uglify.app = {
    src: ['web/static/js/app.js'],
    dest: 'web/static/js/app.js'
}

config.copy = {};

config.copy.app = {
    cwd: 'src/AppBundle/Resources/public/img',
    src: '**',
    dest: 'web/static/img/app',
    expand: true,
    flatten: true,
    filter: 'isFile'
}

config.watch = {};

config.watch.app_js = {
    files: ['src/AppBundle/Resources/public/js/*.js'],
    tasks: ['concat:app_js']
}

config.watch.app_css = {
    files: ['src/AppBundle/Resources/public/css/*.less', 'src/AppBundle/Resources/public/css/*.css'],
    tasks: ['less:app_less']
}



