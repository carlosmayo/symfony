module.exports = function (grunt) {

    var gruntConfig = {
        pkg: grunt.file.readJSON('package.json')
    };

    var frontendConfig = require('./src/AppBundle/Resources/config/grunt.js');

    grunt.util._.extend(gruntConfig, frontendConfig);

    grunt.initConfig(gruntConfig);

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('prod', ['less', 'concat', 'uglify', 'cssmin', 'copy']);
    grunt.registerTask('local', ['less', 'concat']);
    grunt.registerTask('default', ['local', 'watch']);

};